/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "si4463.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi1;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */
#define APP_PACKET_LEN		((uint16_t) 64)

si4463_t si4463;
uint8_t incomingBuffer[APP_PACKET_LEN];
uint8_t outgoingBuffer[APP_PACKET_LEN];
//char txdata[]="abcdefghijklmnopqrstuvwxyz123\t";
char txData[]="012345678901234567890123456789012345678901234567890123456789012";
char txdata[]="STR;ABBEV;Abbeville;RTCM3;1004,1005,1006,1008,1012,1019,1020,1\t";
//char txdata[]="STR;ABBEV;Abbeville;RTCM3;1004,1005,1006,1008,1012,1019,1020,1033,1042,1046,1077,1087,1097,1107,1127,1230;2;\t";
bool txFlag;
bool txFlag1;
bool txFlag2;
bool txFlag3;

int byte_64=3;
int i=0;
int msg_id=1000;
int stop_msg_id=9999;
char buffer[64];
uint8_t dataLength=0;


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI1_Init(void);
static void MX_USART1_UART_Init(void);
/* USER CODE BEGIN PFP */

uint8_t SI4463_IsCTS(void);
void SI4463_WriteRead(const uint8_t * pTxData, uint8_t * pRxData, const uint16_t sizeTxData);
void SI4463_SetSDN(void);
void SI4463_ClearSDN(void);
void SI4463_Select(void);
void SI4463_Deselect(void);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
  si4463.IsClearToSend = SI4463_IsCTS;
  si4463.WriteRead = SI4463_WriteRead;
  si4463.Select = SI4463_Select;
  si4463.Deselect = SI4463_Deselect;
  si4463.SetShutdown = SI4463_SetSDN;
  si4463.ClearShutdown = SI4463_ClearSDN;
  si4463.DelayMs = HAL_Delay;

  /* Disable interrupt pin for init Si4463 */
    HAL_NVIC_DisableIRQ(EXTI0_IRQn);

    /* Init Si4463 with structure */
    SI4463_Init(&si4463);

    /* Clear RX FIFO before starting RX packets */
    //SI4463_ClearRxFifo(&si4463);
    /* Start RX mode.
     * SI4463_StartRx() put a chip in non-armed mode in cases:
     * - successfully receive a packet;
     * - invoked RX_TIMEOUT;
     * - invalid receive.
     * For receiveing next packet you have to invoke SI4463_StartRx() again!*/
   // SI4463_StartRx(&si4463, 32, false, false, false);
    /* Enable interrupt pin and */
      HAL_NVIC_EnableIRQ(EXTI0_IRQn);
      /* Clear interrupts after enabling interrupt pin.
       * Without it may be situation when interrupt is asserted but pin not cleared.*/
      SI4463_ClearInterrupts(&si4463);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

//	  SI4463_Transmit(&si4463,(uint8_t *)txdata,APP_PACKET_LEN);
//	  HAL_GPIO_TogglePin(LEDONBOARD_GPIO_Port, LEDONBOARD_Pin);
//	  HAL_Delay(500);
//	  SI4463_GetInterrupts(&si4463);
//	  if (si4463.interrupts.packetSent)
//	  {
//	  	   SI4463_ClearTxFifo(&si4463);
//	  	   txFlag=si4463.interrupts.packetSent;
//	  	   HAL_UART_Transmit(&huart1, txdata, APP_PACKET_LEN, 200);
//	  	   HAL_Delay(500);

//
//	  }


	  if (msg_id < stop_msg_id)
	  {    sprintf(buffer, "%d,<012345678901234567890123456789012345678901234567890123>\n", msg_id);
	       //dataLength=SI4463_GetTxFifoRemainBytes(&si4463);
		   SI4463_Transmit(&si4463,(uint8_t*)buffer,sizeof(buffer));
		   //HAL_GPIO_TogglePin(LEDONBOARD_GPIO_Port, LEDONBOARD_Pin);
		   HAL_Delay(100);


		   SI4463_GetInterrupts(&si4463);
		   if (si4463.interrupts.packetSent)
		   {
			  //dataLength=SI4463_GetTxFifoRemainBytes(&si4463);
		   	  SI4463_ClearTxFifo(&si4463);
		   	  //dataLength=SI4463_GetTxFifoRemainBytes(&si4463);
		   	  txFlag1=si4463.interrupts.packetSent;
		   	  HAL_UART_Transmit(&huart1,(uint8_t*)buffer,sizeof(buffer), 10);
		   	  //HAL_Delay(100);
		   }
	       msg_id+=1;

	  }

//	  while(byte_64>i)
//	  {
//
//		  if (si4463.interrupts.txFifoAlmostEmpty)
//		  {
//			  SI4463_Transmit(&si4463,txData,sizeof(txData));
//			  HAL_GPIO_TogglePin(LEDONBOARD_GPIO_Port, LEDONBOARD_Pin);
//			  HAL_Delay(500);
//			  SI4463_GetInterrupts(&si4463);
//			  if (si4463.interrupts.packetSent){
//			  		SI4463_ClearTxFifo(&si4463);
//			  		txFlag2=si4463.interrupts.packetSent;
//			  		HAL_UART_Transmit(&huart1,txData,sizeof(txData), 200);
//			  	    HAL_Delay(200);
//			  }
//
//			  i=i+1;
//		  }
//
//	   }
//	   i=0;


//	   SI4463_Transmit(&si4463,lastString,sizeof(lastString));
//	   HAL_GPIO_TogglePin(LEDONBOARD_GPIO_Port, LEDONBOARD_Pin);
//	   HAL_Delay(500);
//	   SI4463_GetInterrupts(&si4463);
//	   if (si4463.interrupts.packetSent)
//	   {
//	   	  SI4463_ClearTxFifo(&si4463);
//	   	  txFlag3=si4463.interrupts.packetSent;
//	   	  HAL_UART_Transmit(&huart1,lastString,sizeof(lastString), 200);
//	   	  HAL_Delay(200);
//	   }

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate =  115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LEDONBOARD_GPIO_Port, LEDONBOARD_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, nSEL_Pin|SDN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : LEDONBOARD_Pin */
  GPIO_InitStruct.Pin = LEDONBOARD_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LEDONBOARD_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : nSEL_Pin SDN_Pin */
  GPIO_InitStruct.Pin = nSEL_Pin|SDN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : CTS_Pin */
  GPIO_InitStruct.Pin = CTS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(CTS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : nIRQ_Pin */
  GPIO_InitStruct.Pin = nIRQ_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(nIRQ_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

}

/* USER CODE BEGIN 4 */
uint8_t SI4463_IsCTS(void)
{
	if(HAL_GPIO_ReadPin(CTS_GPIO_Port, CTS_Pin) == GPIO_PIN_SET)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void SI4463_WriteRead(const uint8_t * pTxData, uint8_t * pRxData, const uint16_t sizeTxData)
{
	HAL_SPI_TransmitReceive(&hspi1, pTxData, pRxData, sizeTxData, 100);
}

void SI4463_SetSDN(void)
{
	HAL_GPIO_WritePin(SDN_GPIO_Port, SDN_Pin, GPIO_PIN_SET);
}

void SI4463_ClearSDN(void)
{
	HAL_GPIO_WritePin(SDN_GPIO_Port, SDN_Pin, GPIO_PIN_RESET);
}

void SI4463_Select(void)
{
	HAL_GPIO_WritePin(nSEL_GPIO_Port, nSEL_Pin, GPIO_PIN_RESET);
}

void SI4463_Deselect(void)
{
	HAL_GPIO_WritePin(nSEL_GPIO_Port, nSEL_Pin, GPIO_PIN_SET);
}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
