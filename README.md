- In this project I am using bluepill to interface with si4463 transciever module for transmitting and receiving a data.

## Pin connection of si4463 corresponding to bluepill.

 ```
si4463 Pin                     Bluepill pin
  Gnd                                Gnd
  Vcc                                3.3V
  nSel                               PA1
  SDN                                PA3
  GPIO1(used as CTS)                 PA2
  CLK                                PA5
  MISO                               PA6
  MOSI                               PA7
  nIRQ                               PB0


```
## Si4463 module

![image.png](./image.png)

## Si4463 Tx Circuit

![image-1.png](./image-1.png)

## Si4463 Rx Circuit

![image-2.png](./image-2.png)

## Output & si4463 Api link

- Inside Driver folder we have a si4463 directory which contains our si4463 api that we have taken from the link [si4463 api link](https://github.com/Borchevkin/example_si4463)

- si4463 reference link:- [datasheet](https://www.silabs.com/documents/public/data-sheets/Si4464-63-61-60.pdf), [ApI Description](https://d1.amobbs.com/bbs_upload782111/files_52/ourdev_720108S29ZHI.pdf),  [Ezradio pro](https://www.farnell.com/datasheets/1889753.pdf)
- We have take the static data of 400 bytes and transmit it in the chunks of five packets of length 62 bytes each.
- We successfully transmitted the data and received the data without losing it.

## Output of transmit data in chunks

![image-3.png](./image-3.png)

## Output of Rx data

![Screenshot_4.png](./Screenshot_4.png)

## Logic Analyzer Output

![logicanalyzer.png](./logicanalyzer.png)

## SDR Output

![sdr.png](./sdr.png)

